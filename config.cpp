class CfgPatches
{
	class FractureMod
	{
		requiredAddons[]=
		{
			// ""
		};
	};
};

class CfgMods
{
	class FractureMod
	{
	    type = "mod";
		
	    class defs
	    {
			
			class worldScriptModule		
            {
                value = "";
                files[] = {"@FractureMod/Scripts/4_World"};
            };
        };
    };
};