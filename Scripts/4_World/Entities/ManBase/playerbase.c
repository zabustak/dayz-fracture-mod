//	Created: 16.12.2019
//	Last modified: 23.12.2019
//	Modified by: Adrian Radzikowski
//	Author: Adrian Radzikowski
//	Mail: dgtiarov@protonmail.com

modded class PlayerBase
{
	const float MAX_HEALTH = 100.0;
	const float MAX_HEALTH_BONES = 1350.0;
	private float m_BoneHealthLegs;
    bool m_HasFracturedLegs;

    override void Init()
    {
        super.Init();

		m_BoneHealthLegs = MAX_HEALTH_BONES;
        m_HasFracturedLegs = false;

		RegisterNetSyncVariableInt("m_BoneHealthLegs");
        RegisterNetSyncVariableBool("m_HasFracturedLegs");
    }

	override void EEHitBy(TotalDamageResult damageResult, int damageType, EntityAI source, int component, string dmgZone, string ammo, vector modelPos, float speedCoef)
	{
		super.EEHitBy(damageResult, damageType, source, component, dmgZone, ammo, modelPos, speedCoef);

        float FractureChance = 0.0;
		int FractureSeverity;

        float FractureDamageThresholdMelee = 26.0; 	// 0-100% chance for fracture if damage is within range <threshold, 2*threshold>
        float FractureDamageThresholdOther = 20.0;	// 100% chance if damage is bigger than 2*threshold

		float FractureHealthThresholdFall = 70.0;	// 20-0% chance for a bruise (minor fracture) if current health is within range <threshold, MAX_HEALTH>
													// Any fall damage will result in a fracture if current health is below the threshold

		float CurrentHealth = GetHealth("", "");

		if (damageType == DT_CUSTOM && ammo == "FallDamage") //TODO add player's mass and speed_coeff factors
		{
			if ( (1.0 - (CurrentHealth / MAX_HEALTH) ) >= ( FractureHealthThresholdFall / MAX_HEALTH ) )
			{
				FractureChance = (1.0 - (CurrentHealth / MAX_HEALTH)) / 3.0;
				FractureSeverity = 0;
			}
			else
			{
				FractureChance = 1.0;
				FractureSeverity = 2;
			}

			if (Math.RandomFloat01() < FractureChance)
            {
                FractureLegs(FractureSeverity);
            }
		}
		else if ( damageResult && (dmgZone == "LeftLeg" || dmgZone == "LeftFoot" || dmgZone == "RightLeg" || dmgZone == "RightFoot") )
		{
			float Damage = damageResult.GetHighestDamage("Health");
			
			if (source != null && source.IsMeleeWeapon() && Damage >= FractureDamageThresholdMelee)
			{
                Damage -= FractureDamageThresholdMelee;

                if (Damage >= FractureDamageThresholdMelee)
                {
                    FractureChance = 1.0;
					FractureSeverity = 2;
                }
                else
                {
				    FractureChance = Damage / FractureDamageThresholdMelee;
					FractureSeverity = 1;
                }

                if (Math.RandomFloat01() < FractureChance)
                {
                    FractureLegs(FractureSeverity);
                }
			}
			else if (Damage >= FractureDamageThresholdOther) // Most likely firearms
			{
				Damage -= FractureDamageThresholdOther;

                if (Damage >= FractureDamageThresholdOther)
                {
                    FractureChance = 0.5;
					FractureSeverity = 1;
                }
                else
                {
                    FractureChance = (Damage / FractureDamageThresholdOther) / 2.0;
					FractureSeverity = 1;
                }
                
                if (Math.RandomFloat01() < FractureChance)
                {
                    FractureLegs(FractureSeverity);
                }
			}
		}

	}

    override bool CanSprint()
	{
		if (m_HasFracturedLegs)
		{
			return false;
		}

		return super.CanSprint();
	}

    override void ApplySplint()
	{
		float add_health_coeff = 0.2;

		AddBoneHealth( (MAX_HEALTH_BONES - GetBoneHealth()) * add_health_coeff );
	}

	float GetBoneHealth()
	{
		return m_BoneHealthLegs;
	}

	void AddBoneHealth(float health)
	{
		m_BoneHealthLegs += health;

		if (m_BoneHealthLegs > MAX_HEALTH_BONES)
		{
			m_BoneHealthLegs = MAX_HEALTH_BONES;
		}

		if (m_BoneHealthLegs < 0)
		{
			m_BoneHealthLegs = 0;
		}
	}

    void FractureLegs(int severity) //0 - minor; 1 - major, 2 - fatal
    {
		const float BONE_DAMAGE_MINOR = -250;
		const float BONE_DAMAGE_MAJOR = -550;
		const float BONE_DAMAGE_FATAL = -1200;

        if (severity == 0)
		{
			AddBoneHealth(BONE_DAMAGE_MINOR);
		}
		else if (severity == 1)
		{
			AddBoneHealth(BONE_DAMAGE_MAJOR);
		}
		else if (severity == 2)
		{
			AddBoneHealth(BONE_DAMAGE_FATAL);
		}
    }
}