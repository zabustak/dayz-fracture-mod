//	Created: 18.12.2019
//	Last modified: 
//	Modified by: 
//	Author: Adrian Radzikowski
//	Mail: dgtiarov@protonmail.com

modded class ModifiersManager
{
	override void Init()
	{
		super.Init();

		AddModifier(new BrokenLegsMdfr);
		AddModifier(new BoneRegenMdfr);
	}
}