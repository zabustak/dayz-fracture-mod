//	Created: 18.12.2019
//	Last modified: 
//	Modified by: 
//	Author: Adrian Radzikowski
//	Mail: dgtiarov@protonmail.com

modded class BrokenLegsMdfr
{
	private const float BONE_HEALTH_THRESHOLD = 1000;
	private const float BONE_HEALTH_THRESHOLD_FORCE_FALL = 300;

	override void Init()
	{
		m_TrackActivatedTime = true;
		m_IsPersistent = true;
		m_ID 					= eModifiers.MDF_BROKEN_LEGS;
		m_TickIntervalInactive 	= DEFAULT_TICK_TIME_INACTIVE;
		m_TickIntervalActive 	= DEFAULT_TICK_TIME_ACTIVE;
	}

	override void OnReconnect(PlayerBase player)
	{
		OnActivate(player);
	}

	override protected void OnActivate(PlayerBase player)
	{
		if (player.GetNotifiersManager()) player.GetNotifiersManager().ActivateByType(eCustomNotifiers.NTF_FRACTURE);
		player.RequestSoundEvent(EPlayerSoundEventID.INJURED_HIGH, true);
		player.m_HasFracturedLegs = true;
		player.SetSynchDirty();
	}

	override protected void OnDeactivate(PlayerBase player)
	{
		if (player.GetNotifiersManager()) player.GetNotifiersManager().DeactivateByType(eCustomNotifiers.NTF_FRACTURE);
		player.m_HasFracturedLegs = false;
		player.SetSynchDirty();
	}

	override bool  ActivateCondition(PlayerBase player)
	{
		if (player.GetBoneHealth() < BONE_HEALTH_THRESHOLD)
		{
			return true;
		}
		return false;
	}

	override protected bool DeactivateCondition(PlayerBase player)
	{
		if(player.GetBoneHealth() >= BONE_HEALTH_THRESHOLD)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	override protected void OnTick(PlayerBase player, float deltaT)
	{
		if (player.GetModifiersManager().IsModifierActive(eModifiers.MDF_MORPHINE))
		{
			return;
		}

		if (player.GetBoneHealth() < BONE_HEALTH_THRESHOLD_FORCE_FALL)
		{
			HumanCommandMove CommandMove = player.GetCommand_Move();
			if (CommandMove)
			{
				CommandMove.ForceStance(DayZPlayerConstants.STANCEIDX_PRONE);
			}
		}
	}
}
